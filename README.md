# README #

FF14.LiquidSpellTimers

### What is this repository for? ###
 FF14.LiquidSpellTimers is a plugin for Advanced Combat Tracker that allows the user to create custom spell,action,ability,item,debuff/buff,etc timers for display on screen.

Code is based off the SpellTimers plugin by Anoyetta. Modified to include performance increases, and critical changes that allow it to be used on older machines running older .NETFramework. Along with some added features/flair of my own.
### How do I get set up? ###

* [.NET Framework 4.5](http://www.microsoft.com/en-us/download/details.aspx?id=30653) is required to use this plugin.

1. Download the latest version from [Releases](https://bitbucket.org/liquidize/ff14.liquidspelltimers/downloads).
2. Extract the folder and its contents to your ACT folder, or somewhere on your harddrive.
3. Open ACT as administrator, and add the "FF14.LiquidSpellTimers.dll" as a plugin from that folder.
4. Configure your timers!

### DoT and Buff Timers ###

DoT and Buff timers are allowed, but please make the expression to determine when they occur specific or use RegEx. Use the "gains the effect of" message for these.

### In-game place holders ###

Some in game place holders can be used inside your regular expressions instead of being specific.

| Command       | Usable         | Reason |
| ------------- |:-------------:|  -----:|
| <me> | true | its allowed.|
| <2>~<8>| true| party member selection, is allowed if the options is turned on (party list order not guaranteed!)|
| <petid>| true| usable once your pets unique id is logged to the system.|
| <t> | false | too performance heavy to add to the system.|
| <tt> | false| too performance heavy to add to the system.|
| <ft> | false | too performance heavy to add to the system.|

### Text Commands ###

You are able to use in game text commands to control the function of FF14.LiquidSpellTimers

| Command       | Description          
| ------------- |:-------------
| /lq refresh spells     | refreshes spell list once panel closed.
| /lq refresh telops      | refreshes tickers
| /lq refresh me	| To update the cache of player's name
| /lq refresh pt	| To update the cache of party members name
| /lq refresh pet	| To update the cache of your own pet ID
| /lq changeenabled spells "sample panel" true	| To enable the spells of the specified panel. disabled = false
| /lq changeenabled spells "sample spell" true	| To enable the specified spell. disabled = false
| /lq changeenabled telops "sample ticker" true	| To enable the specified ticker. disabled = false
| /lq changeenabled spells all true	| To enable all of the spells. disabled = false
| /lq changeenabled telops all true	| To enable all of the tickers. disabled = false
| /lq analyze on	| To enable the collection of the combat log. disabled = off