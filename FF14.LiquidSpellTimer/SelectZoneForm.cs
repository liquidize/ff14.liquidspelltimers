﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

   
    public partial class SelectZoneForm : Form
    {
     
        public SelectZoneForm()
        {
            this.InitializeComponent();
            Utility.Translate.TranslateControls(this);

            this.Load += this.FormLoad;
            this.OKButton.Click += this.OKButton_Click;

            this.AllONButton.Click += (s1, e1) =>
            {
                for (int i = 0; i < this.ZonesCheckedListBox.Items.Count; i++)
                {
                    this.ZonesCheckedListBox.SetItemChecked(i, true);
                }
            };

            this.AllOFFButton.Click += (s1, e1) =>
            {
                for (int i = 0; i < this.ZonesCheckedListBox.Items.Count; i++)
                {
                    this.ZonesCheckedListBox.SetItemChecked(i, false);
                }
            };
        }


      
        public string ZoneFilter { get; set; }

       
        private void FormLoad(object sender, EventArgs e)
        {
            var items = this.ZoneFilter.Split(',');

            this.ZonesCheckedListBox.Items.Clear();
            foreach (var item in FF14PluginHelper.GetZoneList())
            {
                this.ZonesCheckedListBox.Items.Add(
                    item,
                    items.Any(x => x == item.ID.ToString()));
            }
        }

   
        private void OKButton_Click(object sender, EventArgs e)
        {
            var items = new List<string>();
            foreach (Zone item in this.ZonesCheckedListBox.CheckedItems)
            {
                items.Add(item.ID.ToString());
            }

            this.ZoneFilter = string.Join(
                ",",
                items.ToArray());
        }
    }
}
