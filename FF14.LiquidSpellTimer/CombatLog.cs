﻿namespace ACT.LiquidSpellTimer
{
    using System;

    /// <summary>
    /// Combat Log
    /// </summary>
    public class CombatLog
    {
        public CombatLog()
        {
            this.Raw = string.Empty;
            this.Actor = string.Empty;
            this.Action = string.Empty;
            this.Target = string.Empty;
            this.LogTypeName = string.Empty;
        }

        public long ID { get; set; }
        public bool IsOrigin { get; set; }
        public DateTime TimeStamp { get; set; }
        public double TimeStampElapted { get; set; }
        public string Raw { get; set; }
        public CombatLogType LogType { get; set; }
        public string LogTypeName { get; set; }
        public string Actor { get; set; }
        public string Target { get; set; }
        public string Action { get; set; }
        public double CastTime { get; set; }
        public double Span { get; set; }
        public decimal HPRate { get; set; }
    }

    /// <summary>
    /// Combat Log Type
    /// </summary>
    public enum CombatLogType
    {
        /// <summary>0:Analysis Start</summary>
        AnalyzeStart = 0,

        /// <summary>1:Analysis End</summary>
        AnalyzeEnd = 1,
        
        /// <summary>2:CastStart</summary>
        CastStart = 2,
        
        /// <summary>3:Action</summary>
        Action = 3,
        
        /// <summary>4:Add</summary>
        Added = 4,
        
        /// <summary>5:HPRate</summary>
        HPRate = 5
    }
}
