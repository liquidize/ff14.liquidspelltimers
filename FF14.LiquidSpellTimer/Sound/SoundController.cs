﻿namespace ACT.LiquidSpellTimer.Sound
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;

    using ACT.LiquidSpellTimer.Utility;
    using Advanced_Combat_Tracker;

    /// <summary>
    /// Sound Controller
    /// </summary>
    public class SoundController
    {
        /// <summary>
        /// sound controller instance
        /// </summary>
        private static SoundController instance;

        /// <summary>
        /// default instance
        /// </summary>
        public static SoundController Default
        {
            get
            {
                if (instance == null)
                {
                    instance = new SoundController();
                }

                return instance;
            }
        }

        /// <summary>
        /// Timestamp checked
        /// </summary>
        private DateTime checkedYukkuriTimeStamp = DateTime.MinValue;

        /// <summary>
        /// Is Yukkuri enabled?
        /// </summary>
        private bool enabledYukkuri;

        /// <summary>
        /// Is it valid?
        /// </summary>
        public bool EnabledYukkuri
        {
            get
            {
                if ((DateTime.Now - this.checkedYukkuriTimeStamp).TotalSeconds >= 10d)
                {
                    if (ActGlobals.oFormActMain.Visible)
                    {
                        this.enabledYukkuri = ActGlobals.oFormActMain.ActPlugins
                            .Where(x =>
                                x.pluginFile.Name.ToUpper() == "ACT.TTSYukkuri.dll".ToUpper() &&
                                x.lblPluginStatus.Text.ToUpper() == "Plugin Started".ToUpper())
                            .Any();

                        this.checkedYukkuriTimeStamp = DateTime.Now;
                    }
                }

                return this.enabledYukkuri;
            }
        }

        public string WaveDirectory
        {
            get
            {
                // ACT Path
                var asm = Assembly.GetEntryAssembly();
                if (asm != null)
                {
                    var actDirectory = Path.GetDirectoryName(asm.Location);
                    var resourcesUnderAct = Path.Combine(actDirectory, @"resources\wav");

                    if (Directory.Exists(resourcesUnderAct))
                    {
                        return resourcesUnderAct;
                    }
                }

                // Get our path
                var selfDirectory = LiquidSpellTimerPlugin.Location ?? string.Empty;
                var resourcesUnderThis = Path.Combine(selfDirectory, @"resources\wav");

                if (Directory.Exists(resourcesUnderThis))
                {
                    return resourcesUnderThis;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Wave file enumeration
        /// </summary>
        /// <returns>
        /// Collection of wave files</returns>
        public WaveFile[] EnumlateWave()
        {
            var list = new List<WaveFile>();

            // Set dummy for unselected
            list.Add(new WaveFile()
            {
                FullPath = string.Empty
            });

            if (Directory.Exists(this.WaveDirectory))
            {
                foreach (var wave in Directory.GetFiles(this.WaveDirectory, "*.wav")
                    .OrderBy(x => x)
                    .ToArray())
                {
                    list.Add(new WaveFile()
                    {
                        FullPath = wave
                    });
                }
            }

            return list.ToArray();
        }

        /// <summary>
        /// Play the sound
        /// </summary>
        /// <param name="source">
        /// Wave to play</param>
        public void Play(
            string source)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(source))
                {
                    return;
                }

                if (this.EnabledYukkuri)
                {
                    ActGlobals.oFormActMain.TTS(source);
                }
                else
                {
                    Task.Run(() =>
                    {
                        // wav?
                        if (source.EndsWith(".wav"))
                        {
                            // If sound exists
                            if (File.Exists(source))
                            {
                                ActGlobals.oFormActMain.PlaySound(source);
                            }
                        }
                        else
                        {
                            ActGlobals.oFormActMain.TTS(source);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ActGlobals.oFormActMain.WriteExceptionLog(
                    ex,
                    Translate.Get("SoundError"));
            }
        }

        /// <summary>
        /// Wave File
        /// </summary>
        public class WaveFile
        {
            /// <summary>
            /// Path of Wave
            /// </summary>
            public string FullPath { get; set; }

            /// <summary>
            /// Name of Wave
            /// </summary>
            public string Name
            {
                get
                {
                    return !string.IsNullOrWhiteSpace(this.FullPath) ?
                        Path.GetFileName(this.FullPath) :
                        string.Empty;
                }
            }

            /// <summary>
            /// ToString()
            /// </summary>
            /// <returns>Name of this wave as string</returns>
            public override string ToString()
            {
                return this.Name;
            }
        }
    }
}
