﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FF14.LiquidSpellTimer")]
[assembly: AssemblyDescription("Ability Recast Timers and Tick System")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Liquid")]
[assembly: AssemblyProduct("FF14.LiquidSpellTimer")]
[assembly: AssemblyCopyright("Copyright (c) 2015 Liquid")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("9342EA67-2705-4FB1-877E-9CC64E6A5C1C")]

[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
