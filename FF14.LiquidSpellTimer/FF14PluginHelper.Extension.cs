﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using ACT.LiquidSpellTimer.Properties;

  
    public static partial class FF14PluginHelper
    {
  
        private static Combatant player;

        private static DateTime lastPlayerDateTime = DateTime.MinValue;
        
        private static double playerInfoRefreshInterval = Settings.Default.PlayerInfoRefreshInterval;
        
        public static Combatant GetPlayer()
        {
            if (player == null ||
                lastPlayerDateTime <= DateTime.MinValue ||
                (DateTime.Now - lastPlayerDateTime).TotalMinutes >= playerInfoRefreshInterval)
            {
                RefreshPlayer();
            }

            return player;
        }
        
        public static void RefreshPlayer()
        {
            var list = FF14PluginHelper.GetCombatantList();
            if (list.Count > 0)
            {
                player = list[0];
                lastPlayerDateTime = DateTime.Now;
            }
        }
        
        public static List<Combatant> GetCombatantListParty()
        {
            var combatListAll = FF14PluginHelper.GetCombatantList();
            
            int partyCount;
            var partyListById = FF14PluginHelper.GetCurrentPartyList(out partyCount).Take(8);

            var combatListParty = new List<Combatant>();

            foreach (var partyMemberId in partyListById)
            {
                if (partyMemberId == 0)
                {
                    continue;
                }

                var partyMember = (
                    from x in combatListAll
                    where
                    x.ID == partyMemberId
                    select
                    x).FirstOrDefault();

                if (partyMember != null)
                {
                    combatListParty.Add(partyMember);
#if DEBUG
                    Debug.WriteLine("<" + combatListParty.Count().ToString() + "> " + partyMember.Name);
#endif
                }
            }

            return combatListParty;
        }
    }
}
