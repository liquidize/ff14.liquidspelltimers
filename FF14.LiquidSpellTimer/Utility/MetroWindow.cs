﻿namespace ACT.LiquidSpellTimer.Utility
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Metro WPF Window
    /// </summary>
    public partial class MetroWindow : Window
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MetroWindow()
        {
            this.ShowInTaskbar = true;
            this.MouseLeftButtonDown += (s, e) =>
            {
                if (e.ButtonState != MouseButtonState.Pressed)
                {
                    return;
                }

                this.DragMove();
            };
        }
    }
}
