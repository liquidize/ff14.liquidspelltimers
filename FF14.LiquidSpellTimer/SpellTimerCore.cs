﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using ACT.LiquidSpellTimer.Properties;
    using ACT.LiquidSpellTimer.Utility;
    using Advanced_Combat_Tracker;

    public class SpellTimerCore
    {
      
        private static SpellTimerCore instance;

       
        public static SpellTimerCore Default
        {
            get
            {
                if (instance == null)
                {
                    instance = new SpellTimerCore();
#if DEBUG
                    Debug.WriteLine("SpellTimerCore");
#endif
                }

                return instance;
            }
        }

        private System.Windows.Forms.Timer RefreshTimer;

       
        private double RefreshInterval;

      
        private LogBuffer LogBuffer;

      
        private DateTime LastFFXIVProcessDateTime;

     
        private List<SpellTimerListWindow> SpellTimerPanels
        {
            get;
            set;
        }

       
        public void Begin()
        {
            
            CombatAnalyzer.Default.Initialize();

          
            this.SpellTimerPanels = new List<SpellTimerListWindow>();

           
            this.LogBuffer = new LogBuffer();

            
            this.RefreshInterval = Settings.Default.RefreshInterval;
            this.RefreshTimer = new System.Windows.Forms.Timer()
            {
                Interval = (int)this.RefreshInterval
            };

            this.RefreshTimer.Tick += this.RefreshTimerOnTick;
            this.RefreshTimer.Start();
        }

        
        public void End()
        {
            
            CombatAnalyzer.Default.Denitialize();

           
            if (this.LogBuffer != null)
            {
                this.LogBuffer.Dispose();
                this.LogBuffer = null;
            }

            
            if (this.RefreshTimer != null)
            {
                this.RefreshTimer.Stop();
                this.RefreshTimer.Dispose();
                this.RefreshTimer = null;
            }

            
            this.ClosePanels();
            OnePointTelopController.CloseTelops();

            
            Settings.Default.Save();
            SpellTimerTable.Save();
            OnePointTelopTable.Default.Save();

           
            instance = null;
        }

       
        private void RefreshTimerOnTick(
            object sender,
            EventArgs e)
        {
#if DEBUG
            var sw = Stopwatch.StartNew();
#endif
            try
            {
                if (this.RefreshTimer != null &&
                    this.RefreshTimer.Enabled)
                {
                    this.RefreshWindow();
                }
            }
            catch (Exception ex)
            {
                ActGlobals.oFormActMain.WriteExceptionLog(
                    ex,
                    Translate.Get("SpellTimerRefreshError"));
            }
            finally
            {
#if DEBUG
                sw.Stop();
                Debug.WriteLine("●Refresh " + sw.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif
                this.RefreshTimer.Interval = (int)this.RefreshInterval;
            }
        }

        
        private void RefreshWindow()
        {
#if DEBUG
            var sw1 = Stopwatch.StartNew();
#endif
            var spellArray = SpellTimerTable.EnabledTable;
            var telopArray = OnePointTelopTable.Default.EnabledTable;
            
            OnePointTelopController.GarbageWindows(telopArray);
            this.GarbageSpellPanelWindows(spellArray);
#if DEBUG
            sw1.Stop();
            Debug.WriteLine("Refresh ClosePanels ->" + sw1.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif

           
#if DEBUG
            var sw7 = Stopwatch.StartNew();
#endif
            if (ActGlobals.oFormActMain == null ||
                !ActGlobals.oFormActMain.Visible)
            {
                this.HidePanels();
                this.RefreshInterval = 1000;
                return;
            }

            if ((DateTime.Now - this.LastFFXIVProcessDateTime).TotalSeconds >= 5.0d)
            {
          
                if (FF14PluginHelper.GetFFXIVProcess == null)
                {
                    this.RefreshInterval = 1000;

                    if (!Settings.Default.OverlayForceVisible)
                    {
                        this.ClosePanels();
                        OnePointTelopController.CloseTelops();
                        return;
                    }
                }

                this.LastFFXIVProcessDateTime = DateTime.Now;
            }

          
            this.RefreshInterval = Settings.Default.RefreshInterval;
#if DEBUG
            sw7.Stop();
            Debug.WriteLine("Refresh Exists FF14 ->" + sw7.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif

          
#if DEBUG
            var sw2 = Stopwatch.StartNew();
#endif
            var logLines = this.LogBuffer.GetLogLines();
#if DEBUG
            sw2.Stop();
            Debug.WriteLine("Refresh GetLog ->" + sw2.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif

         
#if DEBUG
            var sw3 = Stopwatch.StartNew();
#endif
            OnePointTelopController.Match(
                telopArray,
                logLines);
#if DEBUG
            sw3.Stop();
            Debug.WriteLine("Refresh MatchTelop ->" + sw3.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif

          
#if DEBUG
            var sw4 = Stopwatch.StartNew();
#endif
            this.MatchSpells(
                spellArray,
                logLines);
#if DEBUG
            sw4.Stop();
            Debug.WriteLine("Refresh MatchSpell ->" + sw4.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif

#if DEBUG
            var swC = Stopwatch.StartNew();
#endif
            
            TextCommandController.MatchCommand(
                logLines);

            
            if (!Settings.Default.OverlayVisible)
            {
                this.HidePanels();
                OnePointTelopController.HideTelops();
                return;
            }
#if DEBUG
            swC.Stop();
            Debug.WriteLine("Match Command ->" + swC.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif
            
#if DEBUG
            var sw5 = Stopwatch.StartNew();
#endif
            OnePointTelopController.RefreshTelopWindows(telopArray);
#if DEBUG
            sw5.Stop();
            Debug.WriteLine("Refresh RefreshTelopWindows ->" + sw5.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif
            
#if DEBUG
            var sw6 = Stopwatch.StartNew();
#endif
            this.RefreshSpellPanelWindows(spellArray);
#if DEBUG
            sw6.Stop();
            Debug.WriteLine("Refresh RefreshSpellPanelWindows ->" + sw6.Elapsed.TotalMilliseconds.ToString("N4") + "ms");
#endif
        }

     
        private void GarbageSpellPanelWindows(
            SpellTimer[] spells)
        {
            if (this.SpellTimerPanels != null)
            {
                var removeList = new List<SpellTimerListWindow>();
                foreach (var panel in this.SpellTimerPanels)
                {
                   
                    var setting = (
                        from x in PanelSettings.Default.SettingsTable
                        where
                        x.PanelName == panel.PanelName
                        select
                        x).FirstOrDefault();

                    if (setting == null)
                    {
                        setting = PanelSettings.Default.SettingsTable.NewPanelSettingsRow();
                        PanelSettings.Default.SettingsTable.AddPanelSettingsRow(setting);
                    }

                    setting.PanelName = panel.PanelName;
                    setting.Left = panel.Left;
                    setting.Top = panel.Top;

                    if (DateTime.Now.Second == 0)
                    {
                        PanelSettings.Default.Save();
                    }

                    if (!spells.Any(x => x.Panel == panel.PanelName))
                    {
                        ActInvoker.Invoke(() => panel.Close());
                        removeList.Add(panel);
                    }
                }

                foreach (var item in removeList)
                {
                    this.SpellTimerPanels.Remove(item);
                }
            }
        }

        
        private void MatchSpells(
            SpellTimer[] spells,
            string[] logLines)
        {
            Parallel.ForEach(spells, (spell) =>
            {
                var regex = spell.Regex;

             
                foreach (var logLine in logLines)
                {
                   
                    if (!spell.RegexEnabled ||
                        regex == null)
                    {
                        var keyword = spell.KeywordReplaced;
                        if (string.IsNullOrWhiteSpace(keyword))
                        {
                            continue;
                        }

                        
                        if (logLine.ToUpper().Contains(
                            keyword.ToUpper()))
                        {
                         
                            spell.MatchedLog = logLine;

                            spell.SpellTitleReplaced = spell.SpellTitle;
                            spell.MatchDateTime = DateTime.Now;
                            spell.OverDone = false;
                            spell.BeforeDone = false;
                            spell.TimeupDone = false;
                            spell.CompleteScheduledTime = spell.MatchDateTime.AddSeconds(spell.RecastTime);

                         
                            this.Play(spell.MatchSound);
                            this.Play(spell.MatchTextToSpeak);
                        }
                    }
                    else
                    {
                        
                        var match = regex.Match(logLine);
                        if (match.Success)
                        {
                            
                            spell.MatchedLog = logLine;

                            
                            spell.SpellTitleReplaced = match.Result(spell.SpellTitle);

                            spell.MatchDateTime = DateTime.Now;
                            spell.OverDone = false;
                            spell.BeforeDone = false;
                            spell.TimeupDone = false;
                            spell.CompleteScheduledTime = spell.MatchDateTime.AddSeconds(spell.RecastTime);

                     
                            this.Play(spell.MatchSound);

                            if (!string.IsNullOrWhiteSpace(spell.MatchTextToSpeak))
                            {
                                var tts = match.Result(spell.MatchTextToSpeak);
                                this.Play(tts);
                            }
                        }
                    }

               
                    if (spell.MatchDateTime > DateTime.MinValue)
                    {
                        var keywords = new string[] { spell.KeywordForExtendReplaced1, spell.KeywordForExtendReplaced2 };
                        var regexes = new Regex[] { spell.RegexForExtend1, spell.RegexForExtend2 };
                        var timeToExtends = new long[] { spell.RecastTimeExtending1, spell.RecastTimeExtending2 };

                        for (int i = 0; i < 2; i++)
                        {
                            var keywordToExtend = keywords[i];
                            var regexToExtend = regexes[i];
                            var timeToExtend = timeToExtends[i];

                           
                            var match = false;

                            if (!spell.RegexEnabled ||
                                regexToExtend == null)
                            {
                                if (!string.IsNullOrWhiteSpace(keywordToExtend))
                                {
                                    match = logLine.ToUpper().Contains(keywordToExtend.ToUpper());
                                }
                            }
                            else
                            {
                                match = regexToExtend.Match(logLine).Success;
                            }

                            if (!match)
                            {
                                continue;
                            }

                            var now = DateTime.Now;
                            
                            var newSchedule = spell.CompleteScheduledTime.AddSeconds(timeToExtend);
                            spell.BeforeDone = false;

                            if (spell.ExtendBeyondOriginalRecastTime)
                            {
                                if (spell.UpperLimitOfExtension > 0)
                                {
                                    var newDuration = (newSchedule - now).TotalSeconds;
                                    if (newDuration > (double)spell.UpperLimitOfExtension)
                                    {
                                        newSchedule = newSchedule.AddSeconds(
                                            (newDuration - (double)spell.UpperLimitOfExtension) * -1);
                                    }
                                }
                            }
                            else
                            {
                                var newDuration = (newSchedule - now).TotalSeconds;
                                if (newDuration > (double)spell.RecastTime)
                                {
                                    newSchedule = newSchedule.AddSeconds(
                                        (newDuration - (double)spell.RecastTime) * -1);
                                }
                            }

                            spell.MatchDateTime = now;
                            spell.CompleteScheduledTime = newSchedule;
                        }
                    }
                }
                
                if (spell.RepeatEnabled &&
                    spell.MatchDateTime > DateTime.MinValue)
                {
                    if (DateTime.Now >= spell.MatchDateTime.AddSeconds(spell.RecastTime))
                    {
                        spell.MatchDateTime = DateTime.Now;
                        spell.OverDone = false;
                        spell.TimeupDone = false;
                    }
                }
                
                if (spell.OverTime > 0 &&
                    !spell.OverDone &&
                    spell.MatchDateTime > DateTime.MinValue)
                {
                    var over = spell.MatchDateTime.AddSeconds(spell.OverTime);

                    if (DateTime.Now >= over)
                    {
                        this.Play(spell.OverSound);
                        if (!string.IsNullOrWhiteSpace(spell.OverTextToSpeak))
                        {
                            var tts = spell.RegexEnabled && regex != null ?
                                regex.Replace(spell.MatchedLog, spell.OverTextToSpeak) :
                                spell.OverTextToSpeak;
                            this.Play(tts);
                        }

                        spell.OverDone = true;
                    }
                }
                
                if (spell.BeforeTime > 0 &&
                    !spell.BeforeDone &&
                    spell.MatchDateTime > DateTime.MinValue)
                {
                    if (spell.CompleteScheduledTime > DateTime.MinValue)
                    {
                        var before = spell.CompleteScheduledTime.AddSeconds(spell.BeforeTime * -1);

                        if (DateTime.Now >= before)
                        {
                            this.Play(spell.BeforeSound);
                            if (!string.IsNullOrWhiteSpace(spell.BeforeTextToSpeak))
                            {
                                var tts = spell.RegexEnabled && regex != null ?
                                    regex.Replace(spell.MatchedLog, spell.BeforeTextToSpeak) :
                                    spell.BeforeTextToSpeak;
                                this.Play(tts);
                            }

                            spell.BeforeDone = true;
                        }
                    }
                }
                
                if (spell.RecastTime > 0 &&
                    !spell.TimeupDone &&
                    spell.MatchDateTime > DateTime.MinValue)
                {
                    if (spell.CompleteScheduledTime > DateTime.MinValue &&
                        DateTime.Now >= spell.CompleteScheduledTime)
                    {
                        this.Play(spell.TimeupSound);
                        if (!string.IsNullOrWhiteSpace(spell.TimeupTextToSpeak))
                        {
                            var tts = spell.RegexEnabled && regex != null ?
                                regex.Replace(spell.MatchedLog, spell.TimeupTextToSpeak) :
                                spell.TimeupTextToSpeak;
                            this.Play(tts);
                        }

                        spell.TimeupDone = true;
                    }
                }
            }); // end loop spells
        }

      
        private void RefreshSpellPanelWindows(
            SpellTimer[] spells)
        {
            var panelNames = spells.Select(x => x.Panel.Trim()).Distinct();
            foreach (var name in panelNames)
            {
                var w = this.SpellTimerPanels.Where(x => x.PanelName == name).FirstOrDefault();
                if (w == null)
                {
                    w = new SpellTimerListWindow()
                    {
                        Title = "LiquidSpellTimer - " + name,
                        PanelName = name,
                    };

                    this.SpellTimerPanels.Add(w);
                    
                    if (Settings.Default.ClickThroughEnabled)
                    {
                        w.ToTransparentWindow();
                    }

                    w.Show();
                }

                w.SpellTimers = (
                    from x in spells
                    where
                    x.Panel.Trim() == name
                    select
                    x).ToArray();
                
                if (!w.IsDragging)
                {
                    w.RefreshSpellTimer();
                }
            }
        }

     
        public void SetPanelLocation(
            string panelName,
            double left,
            double top)
        {
            if (this.SpellTimerPanels != null)
            {
                var panel = this.SpellTimerPanels
                    .Where(x => x.PanelName == panelName)
                    .FirstOrDefault();

                if (panel != null)
                {
                    panel.Left = left;
                    panel.Top = top;
                }

                var panelSettings = PanelSettings.Default.SettingsTable
                    .Where(x => x.PanelName == panelName)
                    .FirstOrDefault();

                if (panelSettings != null)
                {
                    panelSettings.Left = left;
                    panelSettings.Top = top;
                }
            }
        }
        
        public void GetPanelLocation(
            string panelName,
            out double left,
            out double top)
        {
            left = 10.0d;
            top = 10.0d;

            if (this.SpellTimerPanels != null)
            {
                var panel = this.SpellTimerPanels
                    .Where(x => x.PanelName == panelName)
                    .FirstOrDefault();

                if (panel != null)
                {
                    left = panel.Left;
                    top = panel.Top;
                }
                else
                {
                    var panelSettings = PanelSettings.Default.SettingsTable
                        .Where(x => x.PanelName == panelName)
                        .FirstOrDefault();

                    if (panelSettings != null)
                    {
                        left = panelSettings.Left;
                        top = panelSettings.Top;
                    }
                }
            }
        }
        
        public void ActivatePanels()
        {
            if (this.SpellTimerPanels != null)
            {
                ActInvoker.Invoke(() =>
                {
                    foreach (var panel in this.SpellTimerPanels)
                    {
                        panel.Activate();
                    }
                });
            }
        }
        
        public void HidePanels()
        {
            if (this.SpellTimerPanels != null)
            {
                ActInvoker.Invoke(() =>
                {
                    foreach (var panel in this.SpellTimerPanels)
                    {
                        panel.HideOverlay();
                    }
                });
            }
        }
        
        public void ClosePanels()
        {
            if (this.SpellTimerPanels != null)
            {
                foreach (var panel in this.SpellTimerPanels)
                {
                    var setting = (
                        from x in PanelSettings.Default.SettingsTable
                        where
                        x.PanelName == panel.PanelName
                        select
                        x).FirstOrDefault();

                    if (setting == null)
                    {
                        setting = PanelSettings.Default.SettingsTable.NewPanelSettingsRow();
                        PanelSettings.Default.SettingsTable.AddPanelSettingsRow(setting);
                    }

                    setting.PanelName = panel.PanelName;
                    setting.Left = panel.Left;
                    setting.Top = panel.Top;
                }

                if (this.SpellTimerPanels.Count > 0)
                {
                    PanelSettings.Default.Save();
                }

                ActInvoker.Invoke(() =>
                {
                    foreach (var panel in this.SpellTimerPanels)
                    {
                        panel.Close();
                    }

                    this.SpellTimerPanels.Clear();
                });
            }
        }
        
        public void LayoutPanels()
        {
            if (this.SpellTimerPanels != null)
            {
                ActInvoker.Invoke(() =>
                {
                    foreach (var panel in this.SpellTimerPanels)
                    {
                        var setting = PanelSettings.Default.SettingsTable
                            .Where(x => x.PanelName == panel.PanelName)
                            .FirstOrDefault();

                        if (setting != null)
                        {
                            panel.Left = setting.Left;
                            panel.Top = setting.Top;
                        }
                        else
                        {
                            panel.Left = 10.0d;
                            panel.Top = 10.0d;
                        }
                    }
                });
            }
        }
        
        private void Play(
            string source)
        {
            ACT.LiquidSpellTimer.Sound.SoundController.Default.Play(source);
        }
    }
}
