﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;

    /// <summary>
    /// Update Checker
    /// TODO: Make it work with bitbucket
    /// </summary>
    public static class UpdateChecker
    {
        
        public const string LastestReleaseUrl = @"https://bitbucket.org/liquidize/ff14.liquidspelltimers/downloads";

      
        public static string ProductName
        {
            get
            {
                var product = (AssemblyProductAttribute)Attribute.GetCustomAttribute(
                    Assembly.GetExecutingAssembly(),
                    typeof(AssemblyProductAttribute));

                return product.Product;
            }
        }

      
        public static string Update()
        {
            var r = string.Empty;

            try
            {
                var html = string.Empty;
                
                using (var wc = new WebClient())
                {
                    using (var stream = wc.OpenRead(LastestReleaseUrl))
                    using (var sr = new StreamReader(stream))
                    {
                        html = sr.ReadToEnd();
                    }
                }

                var lastestReleaseVersion = string.Empty;

              
                var pattern = string.Empty;
                pattern += @"<h1 class=""release-title"".*?>.*?";
                pattern += @"<a href="".*?"".*?>";
                pattern += @"(?<ReleaseTitle>.*?)";
                pattern += @"</a>.*?";
                pattern += @"</h1>";
                
                var regex = new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                for (Match m = regex.Match(html); m.Success; m = m.NextMatch())
                {
                    lastestReleaseVersion = m.Groups["ReleaseTitle"].Value.Trim();
                }

                if (string.IsNullOrWhiteSpace(lastestReleaseVersion))
                {
                    return r;
                }

                var values = lastestReleaseVersion.Replace("v", string.Empty).Split('.');
                var remoteVersion = new Version(
                    values.Length > 0 ? int.Parse(values[0]) : 0,
                    values.Length > 1 ? int.Parse(values[1]) : 0,
                    0,
                    values.Length > 2 ? int.Parse(values[2]) : 0);

               
                var currentVersion = Assembly.GetExecutingAssembly().GetName().Version;

                if (remoteVersion <= currentVersion)
                {
                    return r;
                }

                var prompt = string.Empty;
                prompt += String.Format(Utility.Translate.Get("NewVersionReleased"), ProductName) + Environment.NewLine;
                prompt += "now: " + "v" + currentVersion.Major.ToString() + "." + currentVersion.Minor.ToString() + "." + currentVersion.Revision.ToString() + Environment.NewLine;
                prompt += "new: " + lastestReleaseVersion + Environment.NewLine;
                prompt += Environment.NewLine;
                prompt += Utility.Translate.Get("DownloadPrompt");

                if (MessageBox.Show(prompt, ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) !=
                    DialogResult.Yes)
                {
                    return r;
                }

                Process.Start(LastestReleaseUrl);
            }
            catch (Exception ex)
            {
                r = String.Format(Utility.Translate.Get("NewVersionError"), ex.ToString());
            }

            return r;
        }
    }
}
