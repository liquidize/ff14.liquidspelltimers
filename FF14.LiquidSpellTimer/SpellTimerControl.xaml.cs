﻿using System.ServiceModel.Channels;
using System.Windows;

namespace ACT.LiquidSpellTimer
{
    using System.Diagnostics;
    using System.Windows.Controls;
    using System.Windows.Media;

    using ACT.LiquidSpellTimer.Properties;
    using ACT.LiquidSpellTimer.Utility;

    /// <summary>
    /// SpellTimerControl
    /// </summary>
    public partial class SpellTimerControl : UserControl
    {
        
        private static string recastTimeFormat = 
            Settings.Default.EnabledSpellTimerNoDecimal ? "N0" : "N1";

       
        public SpellTimerControl()
        {
#if DEBUG
            Debug.WriteLine("Spell");
#endif
            this.InitializeComponent();
        }

      
        public string SpellTitle { get; set; }
        
        public double RecastTime { get; set; }

        public double Progress { get; set; }

    
        public bool IsReverse { get; set; }

      
        public string BarColor { get; set; }

     
        public string BarOutlineColor { get; set; }

        public int BarWidth { get; set; }
      
        public int BarHeight { get; set; }

      
        public FontInfo FontInfo { get; set; }

       
        public string FontColor { get; set; }

     
        public string FontOutlineColor { get; set; }

       
        private SolidColorBrush FontBrush { get; set; }

     
        private SolidColorBrush FontOutlineBrush { get; set; }

   
        private SolidColorBrush BarBrush { get; set; }

    
        private SolidColorBrush BarBackBrush { get; set; }


        private SolidColorBrush BarOutlineBrush { get; set; }

     
        public void Refresh()
        {
#if false
            var sw = Stopwatch.StartNew();
#endif
            this.Width = this.BarWidth;

      
            var fontColor = string.IsNullOrWhiteSpace(this.FontColor) ?
                Settings.Default.FontColor.ToWPF() :
                this.FontColor.FromHTMLWPF();
            var fontOutlineColor = string.IsNullOrWhiteSpace(this.FontOutlineColor) ?
                Settings.Default.FontOutlineColor.ToWPF() :
                this.FontOutlineColor.FromHTMLWPF();
            var barColor = string.IsNullOrWhiteSpace(this.BarColor) ?
                Settings.Default.ProgressBarColor.ToWPF() :
                this.BarColor.FromHTMLWPF();
            var barBackColor = barColor.ChangeBrightness(0.4d);
            var barOutlineColor = string.IsNullOrWhiteSpace(this.BarOutlineColor) ?
                Settings.Default.ProgressBarOutlineColor.ToWPF() :
                this.BarOutlineColor.FromHTMLWPF();

            this.FontBrush = this.GetBrush(fontColor);
            this.FontOutlineBrush = this.GetBrush(fontOutlineColor);
            this.BarBrush = this.GetBrush(barColor);
            this.BarBackBrush = this.GetBrush(barBackColor);
            this.BarOutlineBrush = this.GetBrush(barOutlineColor);

            var tb = default(OutlineTextBlock);
            var font = this.FontInfo;

       
            tb = this.SpellTitleTextBlock;
            var title = string.IsNullOrWhiteSpace(this.SpellTitle) ? "　" : this.SpellTitle;
            if (tb.Text != title)
            {
                tb.Text = title;
                tb.SetFontInfo(font);
                tb.Fill = this.FontBrush;
                tb.Stroke = this.FontOutlineBrush;
                tb.StrokeThickness = 0.5d * tb.FontSize / 13.0d;
            }

          
            tb = this.RecastTimeTextBlock;
            var recast = this.RecastTime > 0 ?
                this.RecastTime.ToString(recastTimeFormat) :
                this.IsReverse ? "Over" : "Ready";
            if (tb.Text != recast)
            {
                tb.Text = recast;
                tb.SetFontInfo(font);
                tb.Fill = this.FontBrush;
                tb.Stroke = this.FontOutlineBrush;
                tb.StrokeThickness = 0.5d * tb.FontSize / 13.0d;
            }

        
            var foreRect = this.BarRectangle;
            foreRect.Stroke = this.BarBrush;
            foreRect.Fill = this.BarBrush;
            foreRect.Width = this.IsReverse ?
                (double)(this.BarWidth * (1.0d - this.Progress)) :
                (double)(this.BarWidth * this.Progress);
            foreRect.Height = this.BarHeight;
            foreRect.RadiusX = 2.0d;
            foreRect.RadiusY = 2.0d;
            Canvas.SetLeft(foreRect, 0);
            Canvas.SetTop(foreRect, 0);
            

            var backRect = this.BarBackRectangle;
            backRect.Stroke = this.BarBackBrush;
            backRect.Fill = this.BarBackBrush;
            backRect.Width = this.BarWidth;
            backRect.Height = foreRect.Height;
            backRect.RadiusX = 2.0d;
            backRect.RadiusY = 2.0d;
            Canvas.SetLeft(backRect, 0);
            Canvas.SetTop(backRect, 0);

          


            var outlineRect = this.BarOutlineRectangle;
            outlineRect.Stroke = this.BarOutlineBrush;
            outlineRect.StrokeThickness = 1.0d;
            outlineRect.Width = backRect.Width;
            outlineRect.Height = foreRect.Height;
            outlineRect.RadiusX = 2.0d;
            outlineRect.RadiusY = 2.0d;
            Canvas.SetLeft(outlineRect, 0);
            Canvas.SetTop(outlineRect, 0);

        
            this.BarEffect.Color = this.BarBrush.Color.ChangeBrightness(1.05d);

            this.ProgressBarCanvas.Width = backRect.Width;
            this.ProgressBarCanvas.Height = backRect.Height;

#if false
            sw.Stop();
            Debug.WriteLine("Spell Refresh -> " + sw.ElapsedMilliseconds.ToString("N0") + "ms");
#endif
        }
    }
}
