﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms;

    using ACT.LiquidSpellTimer.Properties;
    using Advanced_Combat_Tracker;

    /// <summary>
    /// LiquidSpellTimer Plugin
    /// </summary>
    public class LiquidSpellTimerPlugin : IActPluginV1
    {
        
        public static ConfigPanel ConfigPanel { get; private set; }

      
        private static Button SwitchVisibleButton { get; set; }

     
        public static string Location
        {
            get;
            private set;
        }

        private Label PluginStatusLabel
        {
            get;
            set;
        }

      
        public LiquidSpellTimerPlugin()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (s, e) =>
            {
                try
                {
                    var asm = new AssemblyName(e.Name);

                    var plugin = ActGlobals.oFormActMain.PluginGetSelfData(this);
                    if (plugin != null)
                    {
                        var thisDirectory = plugin.pluginFile.DirectoryName;
                        var path1 = Path.Combine(thisDirectory, asm.Name + ".dll");
                        if (File.Exists(path1))
                        {
                            return Assembly.LoadFrom(path1);
                        }
                    }

                    var pluginDirectory = Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                        @"Advanced Combat Tracker\Plugins");

                    var path = Path.Combine(pluginDirectory, asm.Name + ".dll");

                    if (File.Exists(path))
                    {
                        return Assembly.LoadFrom(path);
                    }
                }
                catch (Exception ex)
                {
                    ActGlobals.oFormActMain.WriteExceptionLog(
                        ex,
                        Utility.Translate.Get("ACTAssemblyError"));
                }

                return null;
            };
        }

     
        void IActPluginV1.InitPlugin(
            TabPage pluginScreenSpace,
            Label pluginStatusText)
        {
            try
            {
                pluginScreenSpace.Text = "LiquidSpellTimer";
                this.PluginStatusLabel = pluginStatusText;

                
               // this.Update();

               
                var plugin = ActGlobals.oFormActMain.PluginGetSelfData(this);
                if (plugin != null)
                {
                    LiquidSpellTimerPlugin.Location = plugin.pluginFile.DirectoryName;
                }

           
                ConfigPanel = new ConfigPanel();
                pluginScreenSpace.Controls.Add(ConfigPanel);
                ConfigPanel.Size = pluginScreenSpace.Size;
                ConfigPanel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;

               
                SpellTimerTable.Backup();
                OnePointTelopTable.Default.Backup();
                PanelSettings.Default.Backup();

            
                SpellTimerCore.Default.Begin();

                this.SetSwitchVisibleButton();
                this.PluginStatusLabel.Text = "Plugin Started";
            }
            catch (Exception ex)
            {
                ActGlobals.oFormActMain.WriteExceptionLog(
                    ex,
                    Utility.Translate.Get("ACTPluginStartError"));

                this.PluginStatusLabel.Text = "Plugin Initialize Error";
            }
        }

       
        void IActPluginV1.DeInitPlugin()
        {
            try
            {
                SpellTimerCore.Default.End();
                this.RemoveSwitchVisibleButton();
                this.PluginStatusLabel.Text = "Plugin Exited";
            }
            catch (Exception ex)
            {
                ActGlobals.oFormActMain.WriteExceptionLog(
                    ex,
                    Utility.Translate.Get("ACTPluginStopError"));

                this.PluginStatusLabel.Text = "Plugin Exited Error";
            }
        }

     
        private void SetSwitchVisibleButton()
        {
            var changeColor = new Action<Button>((button) =>
            {
                if (Settings.Default.OverlayVisible)
                {
                    button.BackColor = Color.LawnGreen;
                    button.ForeColor = Color.Black;
                }
                else
                {
                    button.BackColor = SystemColors.Control;
                    button.ForeColor = Color.Black;
                }
            });

            SwitchVisibleButton = new Button();
            SwitchVisibleButton.Name = "LiquidSpellTimerSwitchVisibleButton";
            SwitchVisibleButton.Size = new Size(115, 24);
            SwitchVisibleButton.Text = Utility.Translate.Get("LiquidSpellTimers");
            SwitchVisibleButton.TextAlign = ContentAlignment.MiddleCenter;
            SwitchVisibleButton.UseVisualStyleBackColor = true;
            SwitchVisibleButton.Click += (s, e) =>
            {
                var button = s as Button;

                Settings.Default.OverlayVisible = !Settings.Default.OverlayVisible;
                Settings.Default.Save();

                SpellTimerCore.Default.ClosePanels();
                OnePointTelopController.CloseTelops();

                FF14PluginHelper.RefreshPlayer();
                LogBuffer.RefreshPTList();
                LogBuffer.RefreshPetID();

                if (Settings.Default.OverlayVisible)
                {
                    SpellTimerCore.Default.ActivatePanels();
                    OnePointTelopController.ActivateTelops();
                }

                changeColor(s as Button);
            };

            changeColor(SwitchVisibleButton);

            ActGlobals.oFormActMain.Resize += this.oFormActMain_Resize;
            ActGlobals.oFormActMain.Controls.Add(SwitchVisibleButton);
            ActGlobals.oFormActMain.Controls.SetChildIndex(SwitchVisibleButton, 1);

            this.oFormActMain_Resize(this, null);
        }

       
        private void RemoveSwitchVisibleButton()
        {
            if (SwitchVisibleButton != null)
            {
                ActGlobals.oFormActMain.Resize -= this.oFormActMain_Resize;
                ActGlobals.oFormActMain.Controls.Remove(SwitchVisibleButton);
            }
        }

       
        private void oFormActMain_Resize(object sender, EventArgs e)
        {
            SwitchVisibleButton.Location = new Point(
                ActGlobals.oFormActMain.Width - 533,
                0);
        }

       
        private void Update()
        {
            if ((DateTime.Now - Settings.Default.LastUpdateDateTime).TotalHours >= 6d)
            {
                var message = UpdateChecker.Update();
                if (!string.IsNullOrWhiteSpace(message))
                {
                    ActGlobals.oFormActMain.WriteExceptionLog(
                        new Exception(),
                        message);
                }

                Settings.Default.LastUpdateDateTime = DateTime.Now;
                Settings.Default.Save();
            }
        }
    }
}
