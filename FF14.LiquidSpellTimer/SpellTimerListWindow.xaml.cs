﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Interop;
    using System.Windows.Media;

    using ACT.LiquidSpellTimer.Properties;
    using ACT.LiquidSpellTimer.Utility;

    /// <summary>
    /// SpellTimerList Window
    /// </summary>
    public partial class SpellTimerListWindow : Window
    {
        /// <summary>
        /// Constructor definition
        /// </summary>
        public SpellTimerListWindow()
        {
#if DEBUG
            Debug.WriteLine("SpellList");
#endif
            this.InitializeComponent();

            this.SpellTimerControls = new Dictionary<long, SpellTimerControl>();

            this.Loaded += this.SpellTimerListWindow_Loaded;
            this.MouseLeftButtonDown += (s1, e1) => this.DragMove();
            this.Closed += (s1, e1) =>
            {
                if (this.SpellTimerControls != null)
                {
                    this.SpellTimerControls.Clear();
                }
            };

            this.DragOn = new Action<MouseEventArgs>((mouse) =>
            {
                if (mouse.LeftButton == MouseButtonState.Pressed)
                {
                    this.IsDragging = true;
#if DEBUG
                    Debug.WriteLine("Drag On");
#endif
                }
            });

            this.DragOff = new Action<MouseEventArgs>((mouse) =>
            {
                if (mouse.LeftButton == MouseButtonState.Released)
                {
                    this.IsDragging = false;
#if DEBUG
                    Debug.WriteLine("Drag Off");
#endif
                }
            });

            this.MouseDown += (s1, e1) => this.DragOn(e1);
            this.MouseUp += (s1, e1) => this.DragOff(e1);
        }

        /// <summary>
        /// Name of this Panel
        /// </summary>
        public string PanelName { get; set; }

        /// <summary>
        /// Array of SpellTimers on Panel
        /// </summary>
        public SpellTimer[] SpellTimers { get; set; }

        /// <summary>
        /// List of spell timer controls on this panel.
        /// </summary>
        public Dictionary<long, SpellTimerControl> SpellTimerControls { get; private set; }

        /// <summary>
        /// Is the panel being dragged?
        /// </summary>
        public bool IsDragging { get; private set; }

        /// <summary>
        /// Drag Start
        /// </summary>
        private Action<MouseEventArgs> DragOn;

        /// <summary>
        /// Drag Stop
        /// </summary>
        private Action<MouseEventArgs> DragOff;

        /// <summary>Background Color</summary>
        private SolidColorBrush BackgroundBrush { get; set; }

        /// <summary>
        /// Loaded
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="e">Event arguments</param>
        private void SpellTimerListWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Panel position refresh
            var setting = PanelSettings.Default.SettingsTable
                .Where(x => x.PanelName == this.PanelName)
                .FirstOrDefault();

            if (setting != null)
            {
                this.Left = setting.Left;
                this.Top = setting.Top;
            }

            this.RefreshSpellTimer();
        }

        /// <summary>
        /// Refresh drawing of spell timers
        /// </summary>
        public void RefreshSpellTimer()
        {
            if (this.IsDragging)
            {
                return;
            }

            // Hide dispaly if no spell timers to display.
            if (this.SpellTimers == null)
            {
                this.HideOverlay();
                return;
            }

            // Get spelltimers that are using Progressbars
            var spells =
                from x in this.SpellTimers
                where
                x.ProgressBarVisible
                select
                x;

            // Exclude those with no more time remaining.
            if (Settings.Default.TimeOfHideSpell > 0.0d)
            {
                spells =
                    from x in spells
                    where
                    x.DontHide ||
                    (DateTime.Now - x.CompleteScheduledTime).TotalSeconds <= Settings.Default.TimeOfHideSpell
                    select
                    x;
            }

            if (!spells.Any())
            {
                this.HideOverlay();
                return;
            }

            // Sort in order of recast.
            if (Settings.Default.AutoSortEnabled)
            {
                // Ascending?
                if (!Settings.Default.AutoSortReverse)
                {
                    spells =
                        from x in spells
                        orderby
                        x.CompleteScheduledTime,
                        x.DisplayNo
                        select
                        x;
                }
                else
                {
                    spells =
                        from x in spells
                        orderby
                        x.CompleteScheduledTime descending,
                        x.DisplayNo
                        select
                        x;
                }
            }

            // Generate background brush
            if (spells.Count() > 0)
            {
                var s = spells.FirstOrDefault();
                if (s != null)
                {
                    var c = s.BackgroundColor.FromHTMLWPF();
                    var backGroundColor = Color.FromArgb(
                        (byte)s.BackgroundAlpha,
                        c.R,
                        c.G,
                        c.B);

                    this.BackgroundBrush = this.GetBrush(backGroundColor);
                }
            }

            // set background
            var nowbackground = this.BaseColorRectangle.Fill as SolidColorBrush;
            if (nowbackground == null ||
                nowbackground.Color != this.BackgroundBrush.Color)
            {
                if (this.BackgroundBrush != null)
                {
                    this.BaseColorRectangle.Fill = this.BackgroundBrush;
                }
            }

            // Generate a list of spell timer controls
            var displayList = new List<SpellTimerControl>();
            foreach (var spell in spells)
            {
                SpellTimerControl c;
                if (this.SpellTimerControls.ContainsKey(spell.ID))
                {
                    c = this.SpellTimerControls[spell.ID];
                }
                else
                {
                    c = new SpellTimerControl();
                    this.SpellTimerControls.Add(spell.ID, c);

                    c.Visibility = Visibility.Collapsed;
                    c.MouseDown += (s, e) => this.DragOn(e);
                    c.MouseUp += (s, e) => this.DragOff(e);

                    c.HorizontalAlignment = HorizontalAlignment.Left;
                    c.VerticalAlignment = VerticalAlignment.Top;
                    c.Margin = new Thickness(0, 0, 0, 0);

                    this.BaseGrid.RowDefinitions.Add(new RowDefinition());
                    this.BaseGrid.Children.Add(c);

                    c.SetValue(Grid.ColumnProperty, 0);
                    c.SetValue(Grid.RowProperty, this.BaseGrid.Children.Count - 1);
                }

                c.SpellTitle = string.IsNullOrWhiteSpace(spell.SpellTitleReplaced) ?
                    spell.SpellTitle :
                    spell.SpellTitleReplaced;
                c.IsReverse = spell.IsReverse;
                c.RecastTime = 0;
                c.Progress = 1.0d;

                c.BarWidth = spell.BarWidth;
                c.BarHeight = spell.BarHeight;
                c.FontInfo = spell.Font;
                c.FontColor = spell.FontColor;
                c.FontOutlineColor = spell.FontOutlineColor;
                c.BarColor = spell.BarColor;
                c.BarOutlineColor = spell.BarOutlineColor;

                if (spell.MatchDateTime > DateTime.MinValue)
                {
                    c.RecastTime = (spell.CompleteScheduledTime - DateTime.Now).TotalSeconds;
                    if (c.RecastTime < 0)
                    {
                        c.RecastTime = 0;
                    }

                    var totalRecastTime = (spell.CompleteScheduledTime - spell.MatchDateTime).TotalSeconds;
                    c.Progress = totalRecastTime != 0 ?
                        (totalRecastTime - c.RecastTime) / totalRecastTime :
                        1.0d;
                    if (c.Progress > 1.0d)
                    {
                        c.Progress = 1.0d;
                    }
                }

                c.Refresh();

                displayList.Add(c);
            }

            // Spells and Spells to display the same?
            if (displayList.Count == this.BaseGrid.RowDefinitions.Count)
            {
                // Add new row
                this.BaseGrid.RowDefinitions.Add(new RowDefinition());
            }

            // Hide those we dont want to display
            foreach (var c in this.SpellTimerControls)
            {
                if (!spells.Any(x => x.ID == c.Key))
                {
                    c.Value.Visibility = Visibility.Collapsed;
                    c.Value.SetValue(Grid.RowProperty, this.BaseGrid.RowDefinitions.Count - 1);
                }
            }

            // Set display order.
            var index = 0;
            foreach (var displaySpell in displayList)
            {
                displaySpell.SetValue(Grid.RowProperty, index);
                displaySpell.Visibility = Visibility.Visible;
                index++;
            }

            if (spells.Count() > 0)
            {
                this.ShowOverlay();
            }
        }

        #region Measure to ensure proper window focus

        [DllImport("user32.dll")]
        private static extern IntPtr SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_NOACTIVATE = 0x08000000;

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            WindowInteropHelper helper = new WindowInteropHelper(this);
            SetWindowLong(helper.Handle, GWL_EXSTYLE, GetWindowLong(helper.Handle, GWL_EXSTYLE) | WS_EX_NOACTIVATE);
        }

        #endregion
    }
}
