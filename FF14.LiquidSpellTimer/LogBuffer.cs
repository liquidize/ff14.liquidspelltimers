﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using ACT.LiquidSpellTimer.Properties;
    using Advanced_Combat_Tracker;

    
    public class LogBuffer : IDisposable
    {
        private static object lockPetidObject = new object();

        private static bool enabledPartyMemberPlaceHolder = Settings.Default.EnabledPartyMemberPlaceholder;

        private static List<string> ptmember;

    
        private static List<KeyValuePair<string, string>> replacementsByJobs;

        private static string petid;

    
        private static string petidZone;

     
        private List<string> buffer = new List<string>();

       
        public LogBuffer()
        {
            ActGlobals.oFormActMain.OnLogLineRead += this.oFormActMain_OnLogLineRead;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            ActGlobals.oFormActMain.OnLogLineRead -= this.oFormActMain_OnLogLineRead;
            this.Clear();

            GC.SuppressFinalize(this);
        }

        public string[] GetLogLines()
        {
            lock (this.buffer)
            {
                var logLines = this.buffer.ToArray();
                this.buffer.Clear();
                return logLines;
            }
        }

      
        public void Clear()
        {
            lock (this.buffer)
            {
                this.buffer.Clear();

                if (ptmember != null)
                {
                    ptmember.Clear();
                }

#if DEBUG
                Debug.WriteLine("Log was cleared");
#endif
            }
        }
        
        private void oFormActMain_OnLogLineRead(bool isImport, LogLineEventArgs logInfo)
        {
            if (isImport)
            {
                return;
            }

#if false
            Debug.WriteLine(logInfo.logLine);
#endif

            var logLine = logInfo.logLine.Trim();
            
            if (logLine.Contains("にチェンジした。") ||
                logLine.Contains("You change to ") ||
                logLine.Contains("Welcome to"))
            {
                FF14PluginHelper.RefreshPlayer();
                RefreshPTList();
            }
            
            if (enabledPartyMemberPlaceHolder)
            {
                if (ptmember == null ||
                    replacementsByJobs == null ||
                    logLine.Contains("パーティを解散しました。") ||
                    logLine.Contains("がパーティに参加しました。") ||
                    logLine.Contains("がパーティから離脱しました。") ||
                    logLine.Contains("をパーティから離脱させました。") ||
                    logLine.Contains("の攻略を開始した。") ||
                    logLine.Contains("の攻略を終了した。") ||
                    (logLine.Contains("You join ") && logLine.Contains("'s party.")) ||
                    logLine.Contains("You left the party.") ||
                    logLine.Contains("You dissolve the party.") ||
                    logLine.Contains("The party has been disbanded.") ||
                    logLine.Contains("joins the party.") ||
                    logLine.Contains("has left the party.") ||
                    logLine.Contains("was removed from the party."))
                {
                    Task.Run(() =>
                    {
                        Thread.Sleep(5 * 1000);
                        RefreshPTList();
                    });
                }
            }

         
            var player = FF14PluginHelper.GetPlayer();
            if (player != null)
            {
                var jobName = Job.GetJobName(player.Job);
#if DEBUG
                Debug.WriteLine("JOB NAME!! " + jobName);
#endif
                if (jobName == "巴術士" || jobName == "ARC" ||
                    jobName == "学者" || jobName == "SCH" ||
                    jobName == "召喚士" || jobName == "SMN")
                {
                    if (logLine.Contains(player.Name + "の「サモン") ||
                        logLine.Contains("You cast Summon"))
                    {
                        Task.Run(() =>
                        {
                            Thread.Sleep(5 * 1000);
                            RefreshPetID();
                        });
                    }

                    if (petidZone != ActGlobals.oFormActMain.CurrentZone)
                    {
                        Task.Run(() =>
                        {
                            lock (lockPetidObject)
                            {
                                var count = 0;
                                while (petidZone != ActGlobals.oFormActMain.CurrentZone)
                                {
                                    Thread.Sleep(15 * 1000);
                                    RefreshPetID();
                                    count++;

                                    if (count >= 6)
                                    {
                                        petidZone = ActGlobals.oFormActMain.CurrentZone;
                                        break;
                                    }
                                }
                            }
                        });
                    }
                }
            }

            lock (this.buffer)
            {
                this.buffer.Add(logLine);
            }
        }
        
        public static string MakeKeyword(
            string keyword)
        {
            if (string.IsNullOrWhiteSpace(keyword))
            {
                return keyword.Trim();
            }

            if (!keyword.Contains("<") ||
                !keyword.Contains(">"))
            {
                return keyword.Trim();
            }

            keyword = keyword.Trim();

            var player = FF14PluginHelper.GetPlayer();
            if (player != null)
            {
                keyword = keyword.Replace("<me>", player.Name.Trim());
            }

            if (enabledPartyMemberPlaceHolder)
            {
                if (ptmember != null)
                {
                    for (int i = 0; i < ptmember.Count; i++)
                    {
                        keyword = keyword.Replace(
                            "<" + (i + 2).ToString() + ">",
                            ptmember[i].Trim());
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(petid))
            {
                keyword = keyword.Replace("<petid>", petid);
            }
            
            if (replacementsByJobs != null)
            {
                foreach (var replacement in replacementsByJobs)
                {
                    keyword = keyword.Replace(replacement.Key, replacement.Value);
                }
            }

            return keyword;
        }
        
        public static string[] GetPTMember()
        {
            if (ptmember != null)
            {
                return ptmember.ToArray();
            }
            else
            {
                return null;
            }
        }
        
        public static void RefreshPTList()
        {
            if (ptmember == null)
            {
                ptmember = new List<string>();
            }
            else
            {
                ptmember.Clear();
            }

            if (replacementsByJobs == null)
            {
                replacementsByJobs = new List<KeyValuePair<string, string>>();
            }
            else
            {
                replacementsByJobs.Clear();
            }

            if (enabledPartyMemberPlaceHolder)
            {
#if DEBUG
                Debug.WriteLine("PT: Refresh");
#endif
             
                var player = FF14PluginHelper.GetPlayer();
                if (player == null)
                {
                    return;
                }

               
                var combatants = FF14PluginHelper.GetCombatantListParty();

                var sorted =
                    from x in combatants
                    join y in Job.GetJobList() on
                        x.Job equals y.JobId
                    where
                    x.ID != player.ID
                    orderby
                    y.Role,
                    x.Job,
                    x.ID descending
                    select
                    x.Name.Trim();

                foreach (var name in sorted)
                {
                    ptmember.Add(name);
#if DEBUG
                    Debug.WriteLine("<-  " + name);
#endif
                }

             
                if (!combatants.Any())
                {
                    combatants.Add(player);
                }

                foreach (var job in Job.GetJobList())
                {
                    var combatantsByJob = (
                        from x in combatants
                        where
                        x.Job == job.JobId
                        orderby
                        x.ID == player.ID ? 0 : 1,
                        x.ID descending
                        select
                        x).ToArray();

                    if (!combatantsByJob.Any())
                    {
                        continue;
                    }

                    // <JOBn>
                    // ex. <PLD1> → Taro Paladin
                    // ex. <PLD2> → Jiro Paladin
                    for (int i = 0; i < combatantsByJob.Length; i++)
                    {
                        var placeholder = string.Format(
                            "<{0}{1}>",
                            job.JobName,
                            i + 1);

                        replacementsByJobs.Add(new KeyValuePair<string, string>(placeholder.ToUpper(), combatantsByJob[i].Name));
                    }

                    
                    // ex. <PLD> → (?<PLDs>Taro Paladin|Jiro Paladin)
                    var names = string.Join("|", combatantsByJob.Select(x => x.Name).ToArray());
                    var oldValue = string.Format("<{0}>", job.JobName);
                    var newValue = string.Format(
                        "(?<{0}s>{1})",
                        job.JobName.ToUpper(),
                        names);

                    replacementsByJobs.Add(new KeyValuePair<string, string>(oldValue.ToUpper(), newValue));
                }
            }
            
            SpellTimerTable.ClearReplacedKeywords();
            OnePointTelopTable.Default.ClearReplacedKeywords();
        }

        
        public static void RefreshPetID()
        {
            var combatant = FF14PluginHelper.GetCombatantList();

            if (combatant != null &&
                combatant.Count > 0)
            {
                var pet = (
                    from x in combatant
                    where
                    x.OwnerID == combatant[0].ID &&
                    (
                        x.Name.Contains("Fairy") ||
                        x.Name.Contains("- Egi") ||
                        x.Name.Contains("Carbuncle -")
                    )
                    select
                    x).FirstOrDefault();

                if (pet != null)
                {
                    petid = Convert.ToString((long)((ulong)pet.ID), 16).ToUpper();
                    petidZone = ActGlobals.oFormActMain.CurrentZone;

                   
                    SpellTimerTable.ClearReplacedKeywords();
                    OnePointTelopTable.Default.ClearReplacedKeywords();
                }
            }
        }
    }
}
