﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using ACT.LiquidSpellTimer.Properties;
    using ACT.LiquidSpellTimer.Sound;
    using ACT.LiquidSpellTimer.Utility;

    public class OnePointTelopController
    {
        
        private static Dictionary<long, OnePointTelopWindow> telopWindowList = new Dictionary<long, OnePointTelopWindow>();

        public static void SetLocation(
            long telopID,
            double left,
            double top)
        {
            if (telopWindowList != null)
            {
                var telop = telopWindowList.ContainsKey(telopID) ?
                    telopWindowList[telopID] :
                    null;

                if (telop != null)
                {
                    telop.Left = left;
                    telop.Top = top;
                }

                var telopSettings = OnePointTelopTable.Default.Table
                    .Where(x => x.ID == telopID)
                    .FirstOrDefault();

                if (telopSettings != null)
                {
                    telopSettings.Left = left;
                    telopSettings.Top = top;
                }
            }
        }

        public static void GettLocation(
            long telopID,
            out double left,
            out double top)
        {
            left = 0;
            top = 0;

            if (telopWindowList != null)
            {
                var telop = telopWindowList.ContainsKey(telopID) ?
                    telopWindowList[telopID] :
                    null;

                if (telop != null)
                {
                    left = telop.Left;
                    top = telop.Top;
                }
                else
                {
                    var telopSettings = OnePointTelopTable.Default.Table
                        .Where(x => x.ID == telopID)
                        .FirstOrDefault();

                    if (telopSettings != null)
                    {
                        left = telopSettings.Left;
                        top = telopSettings.Top;
                    }
                }
            }
        }

    
        public static void CloseTelops()
        {
            if (telopWindowList != null)
            {
                ActInvoker.Invoke(() =>
                {
                    foreach (var telop in telopWindowList.Values)
                    {
                        telop.DataSource.Left = telop.Left;
                        telop.DataSource.Top = telop.Top;

                        telop.Close();
                    }
                });

                if (telopWindowList.Count > 0)
                {
                    OnePointTelopTable.Default.Save();
                }

                telopWindowList.Clear();
            }
        }

     
        public static void HideTelops()
        {
            if (telopWindowList != null)
            {
                ActInvoker.Invoke(() =>
                {
                    foreach (var telop in telopWindowList.Values)
                    {
                        telop.HideOverlay();
                    }
                });
            }
        }

      
        public static void ActivateTelops()
        {
            if (telopWindowList != null)
            {
                ActInvoker.Invoke(() =>
                {
                    foreach (var telop in telopWindowList.Values)
                    {
                        telop.Activate();
                    }
                });
            }
        }
        
        public static void GarbageWindows(
            OnePointTelop[] telops)
        {
            var removeWindowList = new List<OnePointTelopWindow>();
            foreach (var window in telopWindowList.Values)
            {
                if (!telops.Any(x => x.ID == window.DataSource.ID))
                {
                    removeWindowList.Add(window);
                }
            }

            foreach (var window in removeWindowList)
            {
                ActInvoker.Invoke(() =>
                {
                    window.DataSource.Left = window.Left;
                    window.DataSource.Top = window.Top;
                    window.Close();
                });

                telopWindowList.Remove(window.DataSource.ID);
            }
        }

     
        public static void Match(
            OnePointTelop[] telops,
            string[] logLines)
        {
            Parallel.ForEach(telops, (telop) =>
            {
                var regex = telop.Regex;
                var regexToHide = telop.RegexToHide;

                foreach (var log in logLines)
                {
                   
                    if (regex == null)
                    {
                        var keyword = telop.KeywordReplaced;
                        if (!string.IsNullOrWhiteSpace(keyword))
                        {
                            if (log.ToUpper().Contains(
                                keyword.ToUpper()))
                            {
                                if (!telop.AddMessageEnabled)
                                {
                                    telop.MessageReplaced = telop.Message;
                                }
                                else
                                {
                                    telop.MessageReplaced += string.IsNullOrWhiteSpace(telop.MessageReplaced) ?
                                        telop.Message :
                                        Environment.NewLine + telop.Message;
                                }

                                telop.MatchDateTime = DateTime.Now;
                                telop.Delayed = false;
                                telop.MatchedLog = log;
                                telop.ForceHide = false;

                                SoundController.Default.Play(telop.MatchSound);
                                SoundController.Default.Play(telop.MatchTextToSpeak);

                                continue;
                            }
                        }
                    }

                    
                    if (regex != null)
                    {
                        var match = regex.Match(log);
                        if (match.Success)
                        {
                            if (!telop.AddMessageEnabled)
                            {
                                telop.MessageReplaced = match.Result(telop.Message);
                            }
                            else
                            {
                                telop.MessageReplaced += string.IsNullOrWhiteSpace(telop.MessageReplaced) ?
                                    match.Result(telop.Message) :
                                    Environment.NewLine + match.Result(telop.Message);
                            }

                            telop.MatchDateTime = DateTime.Now;
                            telop.Delayed = false;
                            telop.MatchedLog = log;
                            telop.ForceHide = false;

                            SoundController.Default.Play(telop.MatchSound);
                            if (!string.IsNullOrWhiteSpace(telop.MatchTextToSpeak))
                            {
                                var tts = match.Result(telop.MatchTextToSpeak);
                                SoundController.Default.Play(tts);
                            }

                            continue;
                        }
                    }

                
                    if (regexToHide == null)
                    {
                        var keyword = telop.KeywordToHideReplaced;
                        if (!string.IsNullOrWhiteSpace(keyword))
                        {
                            if (log.ToUpper().Contains(
                                keyword.ToUpper()))
                            {
                                telop.ForceHide = true;
                                continue;
                            }
                        }
                    }

             
                    if (regexToHide != null)
                    {
                        if (regexToHide.IsMatch(log))
                        {
                            telop.ForceHide = true;
                            continue;
                        }
                    }
                }   // end loop logLines

                
                if (!telop.Delayed &&
                    telop.MatchDateTime > DateTime.MinValue &&
                    telop.Delay > 0)
                {
                    var delayed = telop.MatchDateTime.AddSeconds(telop.Delay);
                    if (DateTime.Now >= delayed)
                    {
                        telop.Delayed = true;
                        SoundController.Default.Play(telop.DelaySound);
                        var tts = regex != null && !string.IsNullOrWhiteSpace(telop.DelayTextToSpeak) ?
                            regex.Replace(telop.MatchedLog, telop.DelayTextToSpeak) :
                            telop.DelayTextToSpeak;
                        SoundController.Default.Play(tts);
                    }
                }
            }); // end loop telops
        }
        
        public static void RefreshTelopWindows(
            OnePointTelop[] telops)
        {
            foreach (var telop in telops)
            {
                var w = telopWindowList.ContainsKey(telop.ID) ? telopWindowList[telop.ID] : null;
                if (w == null)
                {
                    w = new OnePointTelopWindow()
                    {
                        Title = "OnePointTelop - " + telop.Title,
                        DataSource = telop
                    };

                    if (Settings.Default.ClickThroughEnabled)
                    {
                        w.ToTransparentWindow();
                    }

                    w.Opacity = 0;
                    w.Topmost = false;
                    w.Show();

                    telopWindowList.Add(telop.ID, w);
                }

            
                if (DateTime.Now.Second == 0)
                {
                    telop.Left = w.Left;
                    telop.Top = w.Top;
                    OnePointTelopTable.Default.Save();
                }

                if (Settings.Default.OverlayVisible &&
                    Settings.Default.TelopAlwaysVisible)
                {
                   
                    if (!w.IsDragging)
                    {
                        w.Refresh();
                        w.ShowOverlay();
                    }

                    continue;
                }

                if (telop.MatchDateTime > DateTime.MinValue)
                {
                    var start = telop.MatchDateTime.AddSeconds(telop.Delay);
                    var end = telop.MatchDateTime.AddSeconds(telop.Delay + telop.DisplayTime);

                    if (start <= DateTime.Now && DateTime.Now <= end)
                    {
                        w.Refresh();
                        w.ShowOverlay();
                    }
                    else
                    {
                        w.HideOverlay();

                        if (DateTime.Now > end)
                        {
                            telop.MatchDateTime = DateTime.MinValue;
                            telop.MessageReplaced = string.Empty;
                        }
                    }

                    if (telop.ForceHide)
                    {
                        w.HideOverlay();
                        telop.MatchDateTime = DateTime.MinValue;
                        telop.MessageReplaced = string.Empty;
                    }
                }
                else
                {
                    w.HideOverlay();
                    telop.MessageReplaced = string.Empty;
                }
            }
        }
    }
}
