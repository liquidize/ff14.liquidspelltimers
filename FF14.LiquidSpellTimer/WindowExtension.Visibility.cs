﻿namespace ACT.LiquidSpellTimer
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    using ACT.LiquidSpellTimer.Properties;

    public static partial class WindowExtension
    {
       
        private static Dictionary<string, SolidColorBrush> brushDictionary = new Dictionary<string, SolidColorBrush>();

        
        public static void ShowOverlay(
            this Window x)
        {
            if (x.Opacity <= 0d)
            {
                var targetOpacity = (100d - Settings.Default.Opacity) / 100d;
                x.Opacity = targetOpacity;
                x.Topmost = true;
            }
        }

       
        public static void HideOverlay(
            this Window x)
        {
            if (x.Opacity > 0d)
            {
                x.Opacity = 0d;
                x.Topmost = false;
            }
        }

    
        public static SolidColorBrush GetBrush(
            this Window x,
            Color color)
        {
            return GetBrush(color);
        }

      
        public static SolidColorBrush GetBrush(
            this UserControl x,
            Color color)
        {
            return GetBrush(color);
        }

     
        private static SolidColorBrush GetBrush(
            Color color)
        {
            if (!brushDictionary.ContainsKey(color.ToString()))
            {
                var brush = new SolidColorBrush(color);
                brush.Freeze();
                brushDictionary[color.ToString()] = brush;
            }

            return brushDictionary[color.ToString()];
        }
    }
}
