﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;

    using ACT.LiquidSpellTimer.Properties;
    using Advanced_Combat_Tracker;

    /// <summary>
    /// Combat Analyzer
    /// </summary>
    public class CombatAnalyzer
    {
        private static readonly string[] CastKeywords = new string[] { "You use", "Stance" };
        private static readonly string[] ActionKeywords = new string[] { "「", "」" };
        private static readonly string[] HPRateKeywords = new string[] { "HP at" };
        private static readonly string[] AddedKeywords = new string[] { "Added new combatant" };

        private static readonly Regex CastRegex = new Regex(
            @"\[.+?\] 00:2[89a]..:(?<actor>.+?)The「(?<skill>.+?)」(You use | Stance)$",
            RegexOptions.Compiled | RegexOptions.ExplicitCapture);

        private static readonly Regex ActionRegex = new Regex(
            @"\[.+?\] 00:2[89a]..:(?<actor>.+?)the「(?<skill>.+?)」$",
            RegexOptions.Compiled | RegexOptions.ExplicitCapture);

        private static readonly Regex HPRateRegex = new Regex(
            @"\[.+?\] ..:(?<actor>.+?) HP at (?<hprate>\d+?)%",
            RegexOptions.Compiled | RegexOptions.ExplicitCapture);

        private static readonly Regex AddedRegex = new Regex(
            @"\[.+?\] 03:Added new combatant (?<actor>.+)\.  ",
            RegexOptions.Compiled | RegexOptions.ExplicitCapture);

        /// <summary>
        /// Combat Analyzer Instance
        /// </summary>
        private static CombatAnalyzer instance;

        /// <summary>
        /// Default Combat Analyzer
        /// </summary>
        public static CombatAnalyzer Default
        {
            get
            {
                if (instance == null)
                {
                    instance = new CombatAnalyzer();
                }

                return instance;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public CombatAnalyzer()
        {
            this.CurrentCombatLogList = new List<CombatLog>();
            this.ActorHPRate = new Dictionary<string, decimal>();
        }

        /// <summary>
        /// Combat Log List
        /// </summary>
        public List<CombatLog> CurrentCombatLogList { get; private set; }

        /// <summary>
        /// Actor HP Rate Dictionary
        /// </summary>
        private Dictionary<string, decimal> ActorHPRate { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        private long id;

        /// <summary>
        /// Initialization
        /// </summary>
        public void Initialize()
        {
            this.CurrentCombatLogList.Clear();
            ActGlobals.oFormActMain.OnLogLineRead += this.oFormActMain_OnLogLineRead;
        }

        /// <summary>
        /// Deinitialize
        /// </summary>
        public void Denitialize()
        {
            ActGlobals.oFormActMain.OnLogLineRead -= this.oFormActMain_OnLogLineRead;
            this.CurrentCombatLogList.Clear();
        }

        /// <summary>
        /// Clear the buffer
        /// </summary>
        public void ClearLogBuffer()
        {
            lock (this.CurrentCombatLogList)
            {
                this.CurrentCombatLogList.Clear();
                this.ActorHPRate.Clear();
            }
        }

        /// <summary>
        /// Analyze Log
        /// </summary>
        public void AnalyzeLog()
        {
            this.AnalyzeLog(this.CurrentCombatLogList);
        }

        /// <summary>
        /// Analyze log using list
        /// </summary>
        /// <param name="logList">ログのリスト</param>
        public void AnalyzeLog(
            List<CombatLog> logList)
        {
            if (logList == null ||
                logList.Count < 1)
            {
                return;
            }

            var previouseAction = new Dictionary<string, DateTime>();

            var i = 0L;
            foreach (var log in logList.OrderBy(x => x.ID))
            {
                // Sleep every so often
                if ((i % 10) == 0)
                {
                    Thread.Sleep(1);
                }

                if (log.LogType == CombatLogType.AnalyzeStart ||
                    log.LogType == CombatLogType.AnalyzeEnd ||
                    log.LogType == CombatLogType.HPRate)
                {
                    continue;
                }

                var key = log.LogType.ToString() + "-" + log.Actor + "-" + log.Action;

                // Look for the same action in a previous insertion
                if (previouseAction.ContainsKey(key))
                {
                    log.Span = (log.TimeStamp - previouseAction[key]).TotalSeconds;
                }

                // Adjust
                previouseAction[key] = log.TimeStamp;

                i++;
            }
        }

        /// <summary>
        /// OnLogLineRead
        /// </summary>
        /// <param name="isImport">Import</param>
        /// <param name="logInfo">logInfo</param>
        private void oFormActMain_OnLogLineRead(
            bool isImport,
            LogLineEventArgs logInfo)
        {
            if (!Settings.Default.CombatLogEnabled)
            {
                return;
            }

            if (this.CurrentCombatLogList == null)
            {
                return;
            }

            // contains a pet
            if (logInfo.logLine.Contains("- Egi") ||
                logInfo.logLine.Contains("Fairy") ||
                logInfo.logLine.Contains("Carbuncle"))
            {
                return;
            }

            // Not import
            if (!isImport)
            {
                // Get player information and party list
                var player = FF14PluginHelper.GetPlayer();
                var ptlist = LogBuffer.GetPTMember();

                if (player == null ||
                    ptlist == null)
                {
                    return;
                }

                // Is the player name in the log?
                if (logInfo.logLine.Contains(player.Name))
                {
                    return;
                }

                // is it included in the party list?
                foreach (var name in ptlist)
                {
                    if (logInfo.logLine.Contains(name))
                    {
                        return;
                    }
                }
            }

            // Contains Casted Keywords
            foreach (var keyword in CastKeywords)
            {
                if (logInfo.logLine.Contains(keyword))
                {
                    this.StoreCastLog(logInfo);
                    return;
                }
            }

            // Contains Action Keyword
            foreach (var keyword in ActionKeywords)
            {
                if (logInfo.logLine.Contains(keyword))
                {
                    this.StoreActionLog(logInfo);
                    return;
                }
            }

            // Contains HP Rate keyword
            foreach (var keyword in HPRateKeywords)
            {
                if (logInfo.logLine.Contains(keyword))
                {
                    this.StoreHPRateLog(logInfo);
                    return;
                }
            }

            // Contains keyword
            foreach (var keyword in AddedKeywords)
            {
                if (logInfo.logLine.Contains(keyword))
                {
                    this.StoreAddedLog(logInfo);
                    return;
                }
            }
        }

        /// <summary>
        /// Store the log
        /// </summary>
        /// <param name="log">ログ</param>
        private void StoreLog(
            CombatLog log)
        {
            switch (log.LogType)
            {
                case CombatLogType.AnalyzeStart:
                    log.LogTypeName = "開始";
                    break;

                case CombatLogType.AnalyzeEnd:
                    log.LogTypeName = "終了";
                    break;

                case CombatLogType.CastStart:
                    log.LogTypeName = "準備動作";
                    break;

                case CombatLogType.Action:
                    log.LogTypeName = "アクション";
                    break;

                case CombatLogType.Added:
                    log.LogTypeName = "Added";
                    break;

                case CombatLogType.HPRate:
                    log.LogTypeName = "残HP率";
                    break;
            }

            lock (this.CurrentCombatLogList)
            {
                // Number the ID
                log.ID = this.id;
                this.id++;

                // does it exceed buffer?
                if (this.CurrentCombatLogList.Count >
                    (Settings.Default.CombatLogBufferSize * 1.1m))
                {
                    // Erase overworth
                    var over = (int)(this.CurrentCombatLogList.Count - Settings.Default.CombatLogBufferSize);
                    this.CurrentCombatLogList.RemoveRange(0, over);
                }

                // Seek elapsed time
                if (this.CurrentCombatLogList.Count > 0)
                {
                    log.TimeStampElapted =
                        (log.TimeStamp - this.CurrentCombatLogList.First().TimeStamp).TotalSeconds;
                }
                else
                {
                    log.TimeStampElapted = 0;
                }

                // Set remaining HP Rate
                if (this.ActorHPRate.ContainsKey(log.Actor))
                {
                    log.HPRate = this.ActorHPRate[log.Actor];
                }

                this.CurrentCombatLogList.Add(log);
            }
        }

        /// <summary>
        /// Store spell log
        /// </summary>
        /// <param name="logInfo">log to store</param>
        private void StoreCastLog(
            LogLineEventArgs logInfo)
        {
            var match = CastRegex.Match(logInfo.logLine);
            if (!match.Success)
            {
                return;
            }

            var log = new CombatLog()
            {
                TimeStamp = logInfo.detectedTime,
                Raw = logInfo.logLine,
                Actor = match.Groups["actor"].ToString(),
                Action = match.Groups["skill"].ToString() + " の準備動作",
                LogType = CombatLogType.CastStart
            };

            this.StoreLog(log);
        }

        /// <summary>
        /// Store action log
        /// </summary>
        /// <param name="logInfo">action log to store</param>
        private void StoreActionLog(
            LogLineEventArgs logInfo)
        {
            var match = ActionRegex.Match(logInfo.logLine);
            if (!match.Success)
            {
                return;
            }

            var log = new CombatLog()
            {
                TimeStamp = logInfo.detectedTime,
                Raw = logInfo.logLine,
                Actor = match.Groups["actor"].ToString(),
                Action = match.Groups["skill"].ToString() + " の発動",
                LogType = CombatLogType.Action
            };

            this.StoreLog(log);
        }

        /// <summary>
        /// Store HP rate log
        /// </summary>
        /// <param name="logInfo">log info to store</param>
        private void StoreHPRateLog(
            LogLineEventArgs logInfo)
        {
            var match = HPRateRegex.Match(logInfo.logLine);
            if (!match.Success)
            {
                return;
            }

            var actor = match.Groups["actor"].ToString();

            if (!string.IsNullOrWhiteSpace(actor))
            {
                decimal hprate;
                if (!decimal.TryParse(match.Groups["hprate"].ToString(), out hprate))
                {
                    hprate = 0m;
                }

                this.ActorHPRate[actor] = hprate;
            }
        }

        /// <summary>
        /// Store Added Log
        /// </summary>
        /// <param name="logInfo">Log info</param>
        private void StoreAddedLog(
            LogLineEventArgs logInfo)
        {
            var match = AddedRegex.Match(logInfo.logLine);
            if (!match.Success)
            {
                return;
            }

            var log = new CombatLog()
            {
                TimeStamp = logInfo.detectedTime,
                Raw = logInfo.logLine,
                Actor = match.Groups["actor"].ToString(),
                Action = "Added",
                LogType = CombatLogType.Added
            };

            this.StoreLog(log);
        }
    }
}
