﻿namespace ACT.LiquidSpellTimer
{
    using System;
    using System.IO;

    using ACT.LiquidSpellTimer.Utility;
    using Advanced_Combat_Tracker;


    public class PanelSettings
    {
  
        private static PanelSettings instance;

        
        public static PanelSettings Default
        {
            get
            {
                if (instance == null)
                {
                    instance = new PanelSettings();
                    instance.Load();
                }

                return instance;
            }
        }

      
        private SpellTimerDataSet.PanelSettingsDataTable settingsTable = new SpellTimerDataSet.PanelSettingsDataTable();

       
        public SpellTimerDataSet.PanelSettingsDataTable SettingsTable
        {
            get
            {
                return this.settingsTable;
            }
        }

        public string DefaultFile
        {
            get
            {
                var r = string.Empty;

                r = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    @"liquid\ACT\ACT.LiquidSpellTimer.Panels.xml");

                return r;
            }
        }

       
        public void Load()
        {
            if (File.Exists(this.DefaultFile))
            {
                this.settingsTable.Clear();

                try
                {
                    this.settingsTable.ReadXml(this.DefaultFile);
                }
                catch (Exception ex)
                {
                    ActGlobals.oFormActMain.WriteExceptionLog(
                        ex,
                        Translate.Get("LoadXMLError"));
                }
            }
        }

        public void Save()
        {
            this.settingsTable.AcceptChanges();

            if (this.settingsTable == null ||
                this.settingsTable.Count < 1)
            {
                return;
            }

            var dir = Path.GetDirectoryName(this.DefaultFile);

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            this.settingsTable.WriteXml(this.DefaultFile);
        }

  
        public void Backup()
        {
            var file = this.DefaultFile;

            if (File.Exists(file))
            {
                var backupFile = Path.Combine(
                    Path.GetDirectoryName(file),
                    Path.GetFileNameWithoutExtension(file) + "." + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".bak");

                File.Copy(
                    file,
                    backupFile,
                    true);

              
                foreach (var bak in Directory.GetFiles(Path.GetDirectoryName(file), "*.bak"))
                {
                    var timeStamp = File.GetCreationTime(bak);
                    if ((DateTime.Now - timeStamp).TotalDays >= 3.0d)
                    {
                        File.Delete(bak);
                    }
                }
            }
        }
    }
}
